package edu.depaul.csc472.larkinrsimplecalculator;

import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    String string;
    int num1;
    int num2;


    Button one;
    Button two;
    Button three;
    Button four;
    Button five;
    Button six;
    Button seven;
    Button eight;
    Button nine;
    Button zero;
    Button add;
    Button equal;
    Button cancel;
    EditText display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        one = (Button) findViewById(R.id.one);
        two = (Button) findViewById(R.id.two);
        three = (Button) findViewById(R.id.three);
        four = (Button) findViewById(R.id.four);
        five = (Button) findViewById(R.id.five);
        six = (Button) findViewById(R.id.six);
        seven = (Button) findViewById(R.id.seven);
        eight = (Button) findViewById(R.id.eight);
        nine = (Button) findViewById(R.id.nine);
        zero = (Button) findViewById(R.id.zero);
        add = (Button) findViewById(R.id.add);
        equal = (Button) findViewById(R.id.equal);
        cancel = (Button) findViewById(R.id.cancel);
        display = (EditText) findViewById(R.id.display);

        View.OnClickListener listener = new View.OnClickListener(){
            public void onClick(View click) {
                Editable answer4now = display.getEditableText();
                switch (click.getId()) {
                    case R.id.one:
                        if (num2 != 0) {
                            num2 = 1;
                            display.setText("");
                        }
                        answer4now = answer4now.append(one.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.two:
                        if (num2 != 0) {
                            num2 = 2;
                            display.setText("");
                        }
                        answer4now = answer4now.append(two.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.three:
                        if (num2 != 0) {
                            num2 = 3;
                            display.setText("");
                        }
                        answer4now = answer4now.append(three.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.four:
                        if (num2 != 0) {
                            num2 = 3;
                            display.setText("");
                        }
                        answer4now = answer4now.append(four.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.five:
                        if (num2 != 0) {
                            num2 = 5;
                            display.setText("");
                        }
                        answer4now = answer4now.append(five.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.six:
                        if (num2 != 0) {
                            num2 = 6;
                            display.setText("");
                        }
                        answer4now = answer4now.append(six.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.seven:
                        if (num2 != 0) {
                            num2 = 7;
                            display.setText("");
                        }
                        answer4now = answer4now.append(seven.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.eight:
                        if (num2 != 0) {
                            num2 = 8;
                            display.setText("");
                        }
                        answer4now = answer4now.append(eight.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.nine:
                        if (num2 != 0) {
                            num2 = 9;
                            display.setText("");
                        }
                        answer4now = answer4now.append(nine.getText());
                        display.setText(answer4now);
                        break;
                    case R.id.cancel:
                        num1 = 0;
                        num2 = 0;
                        display.setText("");
                        break;
                    case R.id.add:
                        if (num1 == 0) {
                            num1 = Integer.parseInt(display.getText().toString());
                            display.setText("");
                        } else if (num2 != 0) {
                            num2 = 0;
                            display.setText("");
                        } else {
                            num2 = Integer.parseInt(display.getText().toString());
                            display.setText("");
                            num1 = num1 + num2;
                            display.setText(Integer.toString(num1));
                        }
                        break;
                    case R.id.equal:
                        if (num2 != 0) {
                            display.setText("");
                            num1 = num1 + num2;
                            display.setText(Integer.toString(num1));
                            }
                        else {
                            add();
                        }

                }

            }
        };
        one.setOnClickListener(listener);
        two.setOnClickListener(listener);
        three.setOnClickListener(listener);
        four.setOnClickListener(listener);
        five.setOnClickListener(listener);
        six.setOnClickListener(listener);
        seven.setOnClickListener(listener);
        eight.setOnClickListener(listener);
        nine.setOnClickListener(listener);
        zero.setOnClickListener(listener);
        add.setOnClickListener(listener);
        equal.setOnClickListener(listener);
        cancel.setOnClickListener(listener);
    }

    public void add(){
        num2 =Integer.parseInt((display.getText().toString()));
        display.setText("");
        num1 = num1 + num2;
        display.setText(Integer.toString(num1));
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
